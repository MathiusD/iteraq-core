from .textjsonconversion import *
from .createimage import *

def matrix(matrix, width=None, height=None):
    if width and height:
        jsontext = text2Json(matrix, width, height)
    else:
        try:
            jsontext = matrix
        except:
            return "error"

    try:
        image = matrix2Image(jsontext)
        return image
    except:
        return "error"