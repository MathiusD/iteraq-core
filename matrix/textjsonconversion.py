from PIL import Image
import numpy

def text2Json(text, w, h):
    res = [[-1 for i in range(w)] for j in range(h)]
    splitText = text.split(',')
    if len(splitText) < w * h:
        return "error"
    count = 0
    for i in range(h):
        for j in range(w):
            res[i][j] = splitText[count]
            count += 1
    return res

def json2Text(json, comma=True):
    res = []
    for i in range(len(json)):
        for j in range(len(json[i])):
            res.append(str(json[i][j]))
    if comma:
        resString = ','.join(res)
    else:
        resString = ''.join(res)
    return resString