import numpy
from PIL import Image
import base64
from io import BytesIO

def matrix2Image(json):
    res = [[0 for i in range(len(json[0]))] for j in range(len(json))]
    for i in range(len(res)):
        if len(json[0]) != len(json[i]):
            return "error"
        for j in range(len(res[i])):
            pixel = int(json[i][j])
            if pixel == 1:
                res[i][j] = [0,0,0]
            else:
                res[i][j] = [255,255,255]
    imageArray = numpy.array(res)
    image = Image.fromarray(numpy.uint8(imageArray), "RGB")
    buffer = BytesIO()
    image.save(buffer, format="PNG")
    imageString = base64.b64encode(buffer.getvalue())
    return imageString