# IterαQ - Section Core

Ce dépôt est le dépôt consacré au développement du coeur applicatif applicatif l'équipe IterαQ au sein de la nightcode 2020.

## Auteurs

* Mathilde Ballouhey
* Dimitri Berges
* William Dorion
* Louis Huret
* Nassim Carriere
* Lucas Gambier
* Féry Mathieu
