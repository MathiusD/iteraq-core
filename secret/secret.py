def secret(str_to_modif):
    index=0
    new_str=""
    ignorer=False
    count_identics=1
    sizeChaine=len(str_to_modif)-1 
    for index in range(sizeChaine):
        if (str_to_modif[index] == str_to_modif[index+1]) and not ignorer:
            ignorer = True
        else:
            if not ignorer:
                new_str = new_str + str_to_modif[index]
            else:
                ignorer = False

    index=sizeChaine-1
    if str_to_modif[sizeChaine] != str_to_modif[sizeChaine-1]:
        new_str = new_str + str_to_modif[len(str_to_modif)-1]
    else:
        while index > 0 and str_to_modif[sizeChaine] == str_to_modif[index]:
            if(str_to_modif[index] == str_to_modif[index+1]):
                count_identics += 1
            index-=1
        if count_identics % 2 == 1:
            new_str = new_str + str_to_modif[sizeChaine]
    return new_str