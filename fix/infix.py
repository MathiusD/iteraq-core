def infix(chaine):
    calcul = chaine.split(' ')
    if (len(calcul) % 2) == 0:
        return "error"

    for i in range(len(calcul)) :
        val=calcul[i]
        if i % 2 == 1:
            test=True
            if val == '+':
                test=False
            if val == '-':
                test=False
            if test: 
                return "error"  
        else:
            if not calcul[i].isnumeric():
                return "error"
    
    res = int(calcul[0])
    for i in range(len(calcul)) :
        
        if calcul[i] == '+' :
            res = res + int(calcul[i+1])
        if calcul[i] == '-' :
            res = res - int(calcul[i+1])
    return res