def postfix(operation):
    lst_operation = operation.split()
    lst_nb = []
    lst_operateurs = []
    for car in lst_operation:
        if(car == '+') or (car == '-'):
            lst_operateurs.append(car)
        else :
            lst_nb.append(car)
    if(len(lst_nb) != len(lst_operateurs) + 1):
        return 'error'
    lst_nb.clear()
    lst_operateurs.clear()
    res = 0
    lst_operation = operation.split()
    lst_nb = []
    for car in lst_operation:
        if(car == '+'):
            try:
                res = res + int(lst_nb.pop()) + int(lst_nb.pop())
                lst_nb.append(res)
                res = 0
            except:
                return 'error'
        elif (car == '-'):
            try:
                res = res - int(lst_nb.pop()) + int(lst_nb.pop())
                lst_nb.append(res)
                res = 0
            except:
                return 'error'
        else :
            lst_nb.append(car)
    return lst_nb.pop()